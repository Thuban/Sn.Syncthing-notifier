Sn -Syncthing notifier
======================

[Syncthing](https://syncthing.net/) is an amazing decentralized data
sharing.

syncthing-notifier show in the systray an icon according to syncthing
status and can alert you if someone wants to share something with you.

Left click on the icon to pen syncthing in a browser.
Right click on it to see a dialog to close syncthing-notifier.

![syncthing-notifier screenshot](sn1.png)

Usage
-------

You only need ``syncthing-notifier`` file and some libraries installed : 


- python3
- py3-tkinter
- py3-Pillow ou py3-PIL
- tktray

Under debian : 

	python3-pil python3-tk python3-pil.imagetk tk-tktray


That's it, no need to download icons, they are included in the file.

Finally, run ``./syncthing-notifier`` to see notifications.


